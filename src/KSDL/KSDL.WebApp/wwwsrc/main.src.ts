import Vue from 'vue';
import App from './Home/App.vue';

new Vue({
  el: '#app',
  render: h => h(App)
});