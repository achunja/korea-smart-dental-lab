var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import Vue from "vue";
import Component from "vue-class-component";
import axios from 'axios';
import IyuString from '@iyu/StringExtensions';
import * as AppCore from '@models/appCore';
import VueRouter from "vue-router";
Vue.use(VueRouter);
var router = new VueRouter({
    mode: 'history',
    routes: []
});
var App = /** @class */ (function (_super) {
    __extends(App, _super);
    function App() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.isBusy = false;
        _this.emailDisabled = true;
        _this.email = '';
        _this.username = '';
        _this.password = '';
        _this.confirmPassword = '';
        _this.errors = [];
        return _this;
    }
    App.prototype.mounted = function () {
        if (this.$route.query.oauthKey == undefined) {
            this.email = this.$route.query.email;
        }
        else {
            this.emailDisabled = false;
        }
    };
    App.prototype.validation = function () {
        this.errors = [];
        if (IyuString.IsNullOrEmpty(this.email)) {
            this.errors.push('required - email');
        }
        if (IyuString.IsNullOrEmpty(this.username)) {
            this.errors.push('required - username');
        }
        if (IyuString.IsNullOrEmpty(this.password)) {
            this.errors.push('required - password');
        }
        if (this.password != this.confirmPassword) {
            this.errors.push('confirm passwords do not match');
        }
        return this.errors.length < 1;
    };
    App.prototype.submit = function () {
        var _this = this;
        if (this.validation()) {
            this.isBusy = true;
            axios.post('/account/register', {
                'token': this.$route.query.token,
                'email': this.email,
                'username': this.username,
                'password': this.password,
                'oauth': this.$route.query.oauth,
                'oauthKey': this.$route.query["oauth-key"]
            })
                .then(function (res) {
                _this.isBusy = false;
                document.location.replace("/app");
            })
                .catch(function (e) {
                if (e.response.data.Code == AppCore.ErrorCodes.DuplicateAccount) {
                    _this.errors = [];
                    _this.errors.push('중복된 username 으로 사용 할 수 없습니다.');
                }
                _this.isBusy = false;
            });
        }
    };
    App = __decorate([
        Component({
            name: "App",
            router: router
        })
    ], App);
    return App;
}(Vue));
export default App;
