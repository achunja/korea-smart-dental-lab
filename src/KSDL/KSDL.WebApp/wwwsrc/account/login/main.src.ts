import $ from 'jquery';
require('../../../wwwroot/lib/semantic/dist/semantic.min');

import Vue from 'vue';
import App from "./App.vue";
import VueEngine from "@iyu/VueEngine";

var vue = new Vue({
  render: h => h(App)
}).$mount('#app')

VueEngine.UI.init(vue);

//initialize...
$('.product-title').transition('hide');
$('.login-panel').transition('hide');

$(document).ready(() => {
	
    setTimeout(() => {
        $('.product-title').transition({
            animation: 'fade down',
            duration: 1000
        });
    }, 1000);

    setTimeout(() => {
        $('.login-panel').transition({
            animation: 'fade left',
            duration: 1000
        });                
    }, 2000);            
});