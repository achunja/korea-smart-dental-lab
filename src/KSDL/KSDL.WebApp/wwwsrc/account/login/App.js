var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import Vue from "vue";
import Component from "vue-class-component";
import axios from 'axios';
import VueEngine from "@iyu/VueEngine";
var App = /** @class */ (function (_super) {
    __extends(App, _super);
    function App() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.isBusy = false;
        _this.email = '';
        _this.pw = '';
        _this.backgroundImage = '';
        _this.messageHeader = '';
        _this.messageContent = '';
        return _this;
    }
    App.prototype.created = function () {
        var _this = this;
        VueEngine.UI.setVM("App", this);
        var url = "/api/service/dailyimage/1";
        axios.get(url)
            .then(function (res) {
            _this.backgroundImage = "url('" + res.data[0] + "')";
        });
    };
    App.prototype.mounted = function () {
        this.initNaverLogin();
    };
    App.prototype.onLogin = function () {
        var data = { email: this.email, password: this.pw, rememberMe: false };
        axios.post("/account/login" + location.search, data)
            .then(function (r) {
            location.href = r.data;
        }).catch(function (e) {
            $('#login-form').form("add errors", ['아이디 또는 패스워드가 잘못되었습니다.']);
        });
    };
    App.prototype.initNaverLogin = function () {
    };
    App.prototype.onLoginNaver = function () {
        var url = "/account/naver-login";
        axios.get(url)
            .then(function (res) {
            document.location.replace(res.data);
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    App.prototype.onLoginGoogle = function () {
        var url = "/account/oauth?provider=Google";
        axios.get(url)
            .then(function (res) {
            document.location.replace(res.data);
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    App.prototype.onRegister = function () {
        var _this = this;
        var elModal = $(this.$refs.registerModal);
        elModal.modal({
            onApprove: function () {
                $VM.App.isBusy = true;
                var url = "/account/register-demand?email=" + _this.email;
                axios.get(url)
                    .then(function (res) {
                    $VM.App.isBusy = false;
                    _this.messageHeader = "이메일 주소 확인";
                    _this.messageContent = "\uC778\uC99D \uBA54\uC77C\uC774 " + _this.email + "(\uC73C)\uB85C \uC804\uC1A1\uB418\uC5C8\uC2B5\uB2C8\uB2E4. \uC774\uBA54\uC77C\uC744 \uD655\uC778 \uD558\uC2DC\uBA74 \uB4F1\uB85D\uC744 \uC9C4\uD589 \uD560 \uC218 \uC788\uC2B5\uB2C8\uB2E4.";
                    var messageModal = $(_this.$refs.messageModal);
                    messageModal.modal('show');
                })
                    .catch(function (err) {
                    $VM.App.isBusy = false;
                });
            }
        }).modal("show");
    };
    App = __decorate([
        Component({
            name: "App",
        })
    ], App);
    return App;
}(Vue));
export default App;
