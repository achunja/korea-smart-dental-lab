import Vue from "vue";
import Component from "vue-class-component";
import axios from 'axios'

import VueEngine from "@iyu/VueEngine";

declare var $VM: any;

@Component({
  name: "App",
})
export default class App extends Vue {

	isBusy: boolean = false;

	email: string = '';
	pw: string = '';

	backgroundImage: string = ''
	
	messageHeader: string = '';
	messageContent: string = '';

	created() {
		VueEngine.UI.setVM("App", this);

		var url = "/api/service/dailyimage/1";
		axios.get(url)
			.then(res => {
				this.backgroundImage = `url('${res.data[0]}')`;
			});
	}

	mounted() {
		this.initNaverLogin();
	}

	onLogin(): void {
		var data = { email: this.email, password: this.pw, rememberMe: false };
		
		axios.post(`/account/login${location.search}`, data)
		.then(r => {
			location.href = r.data;
		}).catch(e => {
			$('#login-form').form("add errors", [ '아이디 또는 패스워드가 잘못되었습니다.' ]);
		});
	}
	
	initNaverLogin() {
	}

	onLoginNaver() {
		var url = "/account/naver-login";
		axios.get(url)
			.then(function (res) {
				document.location.replace(res.data);				
			})
			.catch(function (err) {
				console.log(err);
			});
	}
	
	onLoginGoogle() {
		var url = "/account/oauth?provider=Google";
		axios.get(url)
			.then(function (res) {
				document.location.replace(res.data);				
			})
			.catch(function (err) {
				console.log(err);
			});		
	}

	onRegister() {
		var elModal: any = $(this.$refs.registerModal);
		elModal.modal({
			onApprove : () => {

				$VM.App.isBusy = true;

				var url = `/account/register-demand?email=${this.email}`;
				axios.get(url)
					.then((res) => {						
						$VM.App.isBusy = false;
						
						this.messageHeader = "이메일 주소 확인";
						this.messageContent = `인증 메일이 ${this.email}(으)로 전송되었습니다. 이메일을 확인 하시면 등록을 진행 할 수 있습니다.`;
						var messageModal: any = $(this.$refs.messageModal);
						messageModal.modal('show');
					})
					.catch((err) => {												
						$VM.App.isBusy = false;
					});
				}
		}).modal("show");
	}
}