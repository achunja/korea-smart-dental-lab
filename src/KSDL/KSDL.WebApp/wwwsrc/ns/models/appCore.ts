export enum ErrorCodes {
    DuplicateAccount    = 30010,
    NotFoundToken       = 30020,
    NotMatchedToken     = 30030,
    NotFoundUser        = 30040
}