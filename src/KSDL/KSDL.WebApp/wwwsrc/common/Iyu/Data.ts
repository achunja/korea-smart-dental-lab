namespace Iyu.Data {

    export abstract class ModelBase {
		constructor(init?: any) {
			if (init != undefined) {
				(<any>Object).assign(this, init);
			}
		}
	}

	export abstract class DbKeyModelBase extends ModelBase {
		_key: string | undefined;
	}	
	
	export abstract class DbTsKeyModelBase extends DbKeyModelBase {
		createdDateTime: Date | undefined;
		updatedDateTime: Date | undefined;
	}
}

export default Iyu.Data;