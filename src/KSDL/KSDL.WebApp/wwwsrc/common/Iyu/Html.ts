export function importJs(src: string, onloaded?: () => void) {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.src = src;
    head.appendChild(script);

    if (onloaded) {
        script.onload = (e) => onloaded();
    }
}

export function importCss(href: string) {
    var head = document.getElementsByTagName('head')[0];
    var link = document.createElement('link');
    link.rel = "stylesheet";
    link.href = href;
    head.appendChild(link);
}
