import Vue from "vue";
import moment from 'moment'

namespace Iyu.VueEngine {

	export interface VueEngineWindow extends Window {	
		$vue: Vue;
		$UI: Iyu.VueEngine.UI;
		$VM: any;
	}	
	
	declare var window: VueEngineWindow;	
	export class UI {
	
		private static _vueRoot: Vue;
		private static appData: any;

		public static ready(): void {

			this.registerFilters();

			var appElement = document.getElementById("app");
			var jsonData = appElement!.getAttribute("r-data");
			if (jsonData) {
				VueEngine.UI.appData = JSON.parse(jsonData);
			} else {
				VueEngine.UI.appData = {
					// mode: 'designer'
					mode: 'viewer',
					view_key: 'af6207c1-ddca-4f8d-a6eb-5a273607d4bf' // view
					// // view_key: '19a6bbb8-0c11-4bb8-8b2c-2a3cb1fa0d8c' // view
					// // view_key: '8dae39bb-1106-4194-8288-fbaf5e045798' // form
					// mode: 'dev'
				};
			}
		}
		static registerFilters(): void {
			
			Vue.filter('onlyDate', (v: any) => {
				
				if (v) {
					return moment(String(v)).format("YYYY-MM-DD");
				}
			});
		}

		public static init(vue: Vue): void {

			UI._vueRoot = vue;
			window.$vue = vue;
			window.$UI = this;
		}
		
		public static getChild(ref: string): any {
			return UI._vueRoot.$refs[ref];
		}		

		public static setVM(name: string, vm: Vue) {
			if (window.$VM === undefined) window.$VM = {};
			window.$VM[name] = vm;
		}

		public static getVM(name: string) {
			return window.$VM[name];
		}

		public static getUrlParam(key: string) {						
			var app = UI.getAppVue();
			var router = (<any>app).$router;
			return router.history.current.params[key];
		}

		public static getAppData(key: string) {
			return this.appData[key];
		}

		private static getAppVue(): Vue {
			return window.$VM["App"];
		}
	}
}

export default Iyu.VueEngine;