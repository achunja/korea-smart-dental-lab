import Vue from "vue";
import { Component, Prop, Watch } from 'vue-property-decorator';
import { createNew } from "@common/Iyu/Core";
import AppBizService from "@modules/AppBizService";
import Models from '@modules/AppModels';

@Component({
    name: "tooth-list"
})
export default class ToothList extends Vue {

    @Prop({type: Array, default: []})
    items: Array<ToothItem> = [];
    newItem: ToothItem = new ToothItem();
    dialog: boolean = false;
    e1 = 0;

    selectorItemsLv1: SelectorItem[] = [];
    selectorItemsLv2: SelectorItem[] = [];
    selectorItemsLv3: SelectorItem[] = [];
    selectorItemsLv4: SelectorItem[] = [];
    selectorItemsLv5: SelectorItem[] = [];
    selectorItemsLv6: SelectorItem[] = [];
    selectorItemsLv7: SelectorItem[] = [];
    
    selectItemLv1: Models.MasterDentalListItem | null = null;
    selectItemLv2: Models.MasterDentalListItem | null = null;
    selectItemLv3: Models.MasterDentalListItem | null = null;
    selectItemLv4: Models.MasterDentalListItem | null = null;
    selectItemLv5: Models.MasterDentalListItem | null = null;
    selectItemLv6: Models.MasterDentalListItem | null = null;
    selectItemLv7: Models.MasterDentalListItem | null = null;

    @Watch('selectItemLv1') onChanged_selectItemLv1() { this.resetSelector(1); }
    @Watch('selectItemLv2') onChanged_selectItemLv2() { this.resetSelector(2); }
    @Watch('selectItemLv3') onChanged_selectItemLv3() { this.resetSelector(3); }
    @Watch('selectItemLv4') onChanged_selectItemLv4() { this.resetSelector(4); }
    @Watch('selectItemLv5') onChanged_selectItemLv5() { this.resetSelector(5); }
    @Watch('selectItemLv6') onChanged_selectItemLv6() { this.resetSelector(6); }
    @Watch('selectItemLv7') onChanged_selectItemLv7() { this.resetSelector(7); }

    allItems: Models.MasterDentalListItem[] = [];

    shadeItems: Array<string> = ["INCISAL", "BADY&CERVICAL"];
    selectedShadeItem: string = '';
    shadeText: string = '';

    occlusalDesignItems: Array<string> = ["FULL PORCELAIN", "3/4 METAL OCCLUSAL", "BUCCAL PORCELAIN"];
    selectedOcclusalDesignItem: string = '';

    etcItems: Array<string> = ["신경치료", "변색치"];
    selectedEtcItem: string = '';

    memo: string = '';

    resetSelector(lv: number) {

        console.debug('resetSelector');

        if (lv === 1) {
            this.selectorItemsLv2 = [];
            this.allItems.filter(p => p.level === 2 && p.parent_key == this.selectItemLv1!._key).forEach((item) => {
                this.selectorItemsLv2.push(createNew(SelectorItem, { text: item.text, value: item.value, _key: item._key }));
            });
            this.selectorItemsLv3 = [];
            this.selectorItemsLv4 = [];
            this.selectorItemsLv5 = [];
            this.selectorItemsLv6 = [];
            this.selectorItemsLv7 = [];
        } else if (lv === 2) {
            this.selectorItemsLv3 = [];
            this.allItems.filter(p => p.level === 3 && p.parent_key == this.selectItemLv2!._key).forEach((item) => {
                this.selectorItemsLv3.push(createNew(SelectorItem, { text: item.text, value: item.value, _key: item._key }));
            });
            this.selectorItemsLv4 = [];
            this.selectorItemsLv5 = [];
            this.selectorItemsLv6 = [];
            this.selectorItemsLv7 = [];
        } else if (lv === 3) {
            this.selectorItemsLv4 = [];
            this.allItems.filter(p => p.level === 4 && p.parent_key == this.selectItemLv3!._key).forEach((item) => {
                this.selectorItemsLv4.push(createNew(SelectorItem, { text: item.text, value: item.value, _key: item._key }));
            });            
            this.selectorItemsLv5 = [];
            this.selectorItemsLv6 = [];
            this.selectorItemsLv7 = [];
        } else if (lv === 4) {
            this.selectorItemsLv5 = [];
            this.allItems.filter(p => p.level === 5 && p.parent_key == this.selectItemLv4!._key).forEach((item) => {
                this.selectorItemsLv5.push(createNew(SelectorItem, { text: item.text, value: item.value, _key: item._key }));
            });            
            this.selectorItemsLv6 = [];
            this.selectorItemsLv7 = [];
        } else if (lv === 5) {
            this.selectorItemsLv6 = [];
            this.allItems.filter(p => p.level === 6 && p.parent_key == this.selectItemLv5!._key).forEach((item) => {
                this.selectorItemsLv6.push(createNew(SelectorItem, { text: item.text, value: item.value, _key: item._key }));
            });            
            this.selectorItemsLv7 = [];
        } else if (lv === 6) {
            this.selectorItemsLv7 = [];
            this.allItems.filter(p => p.level === 7 && p.parent_key == this.selectItemLv6!._key).forEach((item) => {
                this.selectorItemsLv7.push(createNew(SelectorItem, { text: item.text, value: item.value, _key: item._key }));
            });            
        } else if (lv === 7) {
        } else {
            throw "not implements...";
        }
    }

    async created() {

        this.allItems = await AppBizService.getMasterDentalListItemsAsync();
        
        this.allItems.filter(p => p.level === 1).forEach((item) => {
            this.selectorItemsLv1.push(createNew(SelectorItem, { text: item.text, value: item.value, _key: item._key }));
        })
    }
    
    addNewItem(e: MouseEvent) {
        this.dialog = true;
    }

    cancelDialog() {
        this.refreshNewItem();
    }

    okDialog() {
        this.items.push(this.newItem);
        this.refreshNewItem();
    }

    refreshNewItem() {
        this.newItem = new ToothItem();
        this.dialog = false;
        this.e1 = 1;
    }

    enableSelector(lv: number): boolean {
        console.debug(`enableSelector - ${lv}`);
        if (lv < 3) {
            return true;
        } else {
            return false;
        }
    }
}

class ToothItem {
    _key: string = '';
    display: string = 'hello ??';
    toothNumbers: toothNumbersModel = new toothNumbersModel();
}

class toothNumbersModel {
    n11: boolean = false;
    n12: boolean = false;
    n13: boolean = false;
    n14: boolean = false;
    n15: boolean = false;
    n16: boolean = false;
    n17: boolean = false;
    n18: boolean = false;

    n21: boolean = false;
    n22: boolean = false;
    n23: boolean = false;
    n24: boolean = false;
    n25: boolean = false;
    n26: boolean = false;
    n27: boolean = false;
    n28: boolean = false;

    n31: boolean = false;
    n32: boolean = false;
    n33: boolean = false;
    n34: boolean = false;
    n35: boolean = false;
    n36: boolean = false;
    n37: boolean = false;
    n38: boolean = false;

    n41: boolean = false;
    n42: boolean = false;
    n43: boolean = false;
    n44: boolean = false;
    n45: boolean = false;
    n46: boolean = false;
    n47: boolean = false;
    n48: boolean = false;    
}

class SelectorItem {
    public _key: string = '';
    public text: string = '';
    public value: string = '';
}