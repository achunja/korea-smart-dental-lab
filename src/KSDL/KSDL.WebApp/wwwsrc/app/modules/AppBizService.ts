import axios from 'axios';
import Models from './AppModels';

declare var $host: string;
var _host = typeof($host) == 'undefined' ? document.location.origin : $host;

function resolvePath(url: string) {
	return `${_host}${url}`;
}

class AppBizService {
    
    async getAccountInfoAsync(account_key: string) {
        
        console.debug('called BizSrvice - getAccountInfoAsync');
        var url = `/api/account-info/${account_key}`;
        url = resolvePath(url);	
    
        try {
            return await axios.get(url);
        } catch (error) {
            return error
        }
    }

    async saveAccountInfoAsync(accountInfo: Models.AccountInfo) {
        
        console.debug('called BizSrvice - saveAccountInfoAsync');
        var url = `/api/account-info`;
        url = resolvePath(url);	
        try {
            return await axios.post(url, accountInfo);
        } catch (error) {
            return error
        }
    }

    async saveDocDentRequestAsync(doc: Models.DocDentRequest) {

        console.debug('called BizSrvice - saveDocDentRequestAsync');
        var url = `/api/doc-dent-request`;
        url = resolvePath(url);	
        try {
            return await axios.post(url, doc);
        } catch (error) {
            return error
        }        
    }

    async getDocDentRequestsAsync(account_key: string): Promise<Models.DocDentRequest[]> {

        console.debug('called BizSrvice - getDocDentRequestsAsync');
        var url = `/api/doc-dent-requests/${account_key}`;
        url = resolvePath(url);	
    
        try {
            var r = await axios.get(url);
            return r.data;
        } catch (error) {
            return error
        }
    }

    async getMasterDentalListItemsAsync(): Promise<Models.MasterDentalListItem[]> {

        console.debug('called BizSrvice - getMasterDentalListItemsAsync');
        var url = `/api/MasterDentalListItems`;
        url = resolvePath(url);	
    
        try {
            var r = await axios.get(url);
            return r.data;
        } catch (error) {
            return error
        }
    }
    

}

export default new AppBizService();