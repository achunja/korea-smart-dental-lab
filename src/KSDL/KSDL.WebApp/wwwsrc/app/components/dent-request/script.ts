import Vue from "vue";
import { Component, Prop } from 'vue-property-decorator';

import Models from "@modules/AppModels";
import AppFunctions from "@modules/AppFunctions";
import AppBizService from "@modules/AppBizService";

import ToothList from '../../controls/tooth-list/component.vue';

@Component({
    components: {
        ToothList
    }
})
export default class DentRequest extends Vue {

    isBusy = false;
    documentDialog = false;

    accountInfo: Models.AccountInfo = new Models.AccountInfo();

    @Prop({type: Object, default: {}})
    doc: Models.DocDentRequest = new Models.DocDentRequest();
    
    headers = [
        { text: "의뢰일", vaue: "order_date"},
        { text: "납품요구일", vaue: "request_date"},
        { text: "성별", vaue: "patient_sex"},
        { text: "연령", vaue: "patient_age"},
        { text: "성명", vaue: "patient_name"}
    ];
    documents: Models.DocDentRequest[] = [];
    selectedDocuments: Models.DocDentRequest[] = [];

    async created() {
        this.isBusy = true;
        var r = await AppFunctions.getAccountInfoAsync();
        if (r instanceof Models.AccountInfo) {
            this.accountInfo = r;
        }

        await this.FillListAsync();

        this.isBusy = false;
    }

    newDocument() {
        this.doc = new Models.DocDentRequest();
        this.doc.hospitalName = this.accountInfo.hospitalName;
        this.doc.account_key = this.accountInfo.account_key;

        this.documentDialog = true;
    }

    async executeOkCommand() {
        this.isBusy = true;
        await AppBizService.saveDocDentRequestAsync(this.doc);
        await this.FillListAsync();
        this.isBusy = false;
    }

    async FillListAsync() {
        var items = await AppBizService.getDocDentRequestsAsync(this.accountInfo.account_key);
        this.documents = items;
    }

    executeCancelCommand() {
        console.debug('executeCancelCommand');
    }
}