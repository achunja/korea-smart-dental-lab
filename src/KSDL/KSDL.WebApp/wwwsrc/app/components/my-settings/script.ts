import Vue from "vue";
import { Component, Prop } from 'vue-property-decorator';

import Models from "@modules/AppModels";
import AppFunctions from "@modules/AppFunctions";
import AppBizService from "@modules/AppBizService";

@Component({})
export default class MySettings extends Vue {

    accountInfo: Models.AccountInfo = new Models.AccountInfo(); 
    isBusy: boolean = false;

    async created() {
        this.isBusy = true;
        var r = await AppFunctions.getAccountInfoAsync();
        if (r instanceof Models.AccountInfo) {
            this.accountInfo = r;
        }
        this.isBusy = false;
    }

    async executeOkCommand() {

        this.isBusy = true;
        await AppBizService.saveAccountInfoAsync(this.accountInfo);
        this.isBusy = false;
    }

    executeCancelCommand() {
        
        console.log('executeCancelCommand');
    }
}