var wp = require('webpack');
var path = require('path');

module.exports = function(filename) {
    return {
        watch: false,
        module: {
            rules: [
                { test: /\.ts$/, loader: 'ts-loader', options: { appendTsSuffixTo: [/\.vue$/], transpileOnly: true } },
                { test: /\.vue$/, loader: 'vue-loader', options: { esModule: true } },
                { test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader?paths=node_modules' },
                { test: /\.(png|jpg|gif|svg)$/, loader: 'file-loader', options: { name: '[name].[ext]?[hash]' } }
            ]
        },
        resolve: {
            extensions: ['.js', '.json', '.ts', '.vue'],
            alias: {
                'vue$': 'vue/dist/vue.esm.js',
                '@common': path.join(__dirname, 'wwwsrc/common'),
                '@iyu': '@common/Iyu',
                '@modules': path.join(__dirname, 'wwwsrc/app/modules'),
                '@ns': path.join(__dirname, 'wwwsrc/ns'),
                '@models': '@ns/models'
            },
            symlinks: false
        },
        plugins: [
            new wp.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery'
            })
        ],
        output: {
            filename: filename
        }
    };
}