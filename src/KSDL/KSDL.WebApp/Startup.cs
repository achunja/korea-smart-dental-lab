﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Iyu.Core.Asp.ModernApp;
using Iyu.Core.Asp.ModernApp.Models;
using Iyu.Core.Asp.ModernApp.Services;
using Iyu.Data;
using KSDL.WebApp.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;

namespace KSDL.WebApp
{
    public class Startup
    {

        private readonly IHostingEnvironment _hostingEnvironment;

        public Startup(IHostingEnvironment env)
        {
            _hostingEnvironment = env;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            Iyu.Core.Asp.AppManager.Current.GetServiceProvider = () => services.BuildServiceProvider();

            #region iyu services...

            services.AddAppIdentity();
            services.AddBingImageService();
            services.AddSingleton<IBizService, AppBizService>();
            services.AddSingleton(Iyu.Core.Asp.AppManager.Current.ServiceProvider.GetService<IBizService>() as IIdentityBizService);

            #endregion

            #region DEBUG

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            #endregion

            services.AddMvc()
                .AddJsonOptions(options => 
                {
                    options.SerializerSettings.Converters.Add(new Iyu.Core.Asp.JsonConverters.GuidJsonConverter());
                });
           
            var physicalProvider = _hostingEnvironment.ContentRootFileProvider;
            var embeddedProvider = new EmbeddedFileProvider(Assembly.GetEntryAssembly());
            var compositeProvider = new CompositeFileProvider(physicalProvider, embeddedProvider);
            services.AddSingleton<IFileProvider>(compositeProvider);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            app.UseAppManager();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStatusCodePages();

            app.UseAuthentication();

            #region DEBUG

            app.UseCors("AllowAll");
            
            #endregion

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseDefaultFiles();
            app.UseStaticFiles();

            var provider = new FileExtensionContentTypeProvider();
            provider.Mappings.Add(".exe", "application/x-msdownload");
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot", "publish")),
                RequestPath = "/publish",
                ContentTypeProvider = provider
            });
        }
    }
}
