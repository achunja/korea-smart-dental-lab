﻿using Iyu.Core.Asp.ModernApp;
using Iyu.Core.Asp.ModernApp.Models;
using Iyu.Core.Asp.ModernApp.Models.AccountViewModels;
using Iyu.Data;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace KSDL.WebApp.Services
{
    public class AppBizService : BizServiceBase, IIdentityBizService
    {
        protected override string ConnectionString { get; }

        public AppBizService(IConfiguration configuration)
        {
            ConnectionString = configuration["ConnectionStrings:DefaultConnection"];
        }

        #region properties...

        public DbItemsContext<ApplicationUser> Users { get => GetPropertyTry() ?? SetProperty(new DbItemsContext<ApplicationUser>(this)); }

        #endregion

        #region accounts...

        public async Task<Guid> GetAccountRegisterTokenAsync(string email)
        {
            var accountVerifyToken = await FindOneAsync<AccountVerifyToken>(new SqlCondition<string>("Email", email));

            if (accountVerifyToken == null)
            {
                accountVerifyToken = await InsertOrUpdateAsync(new AccountVerifyToken()
                {
                    Email = email,
                });
            }

            return accountVerifyToken.Token;
        }

        public async Task<AccountVerifyToken> FindAccountVerifyTokenByTokenAsync(Guid token)
        {
            return await FindOneAsync<AccountVerifyToken>(new SqlCondition<Guid>("Token", token));
        }

        public async Task<AccountVerifyToken> FindAccountVerifyTokenByEmailAsync(string email, SqlTransaction transaction = null)
        {
            return await FindOneAsync<AccountVerifyToken>(new SqlCondition<string>("Email", email));
        }

        public async Task<AccountView> GetAccountByUsernameAsync(string username)
        {
            var account = await FindOneAsync<AccountView>(SqlCondition.Create("Username", username));
            if (account == null) return null;

            account.Password = string.Empty;    // 암호는 서버에서만 취급함.
            return account;
        }
       
        public async Task<AccountDomain> InsertAccountDomainAsync(AccountRegisterModel registerModel)
        {
            var email = registerModel.Email;
            var username = registerModel.Username;
            var pw = registerModel.Password;

            using (var broker = SqlBroker.CreateNew(ConnectionString, transaction: true))
            {
                var accountDomain = await broker.FindOneAsync<AccountDomain>(new SqlCondition<string>("Email", email));
                if (accountDomain != null) return accountDomain;

                accountDomain = new AccountDomain()
                {
                    Username = username,
                    Email = email,
                    Password = pw
                };
                accountDomain = await broker.InsertOrUpdateAsync(accountDomain);

                AccountVerifyToken accountVerifyToken;
                if (string.IsNullOrEmpty(registerModel.OAuthKey))
                    accountVerifyToken = await FindAccountVerifyTokenByEmailAsync(email, broker.GetTransaction());
                else
                    accountVerifyToken = await FindOneAsync<AccountVerifyToken>(new SqlCondition<string>("Email", registerModel.OAuthKey));
                accountVerifyToken.Account_Key = accountDomain._key;
                await broker.UpdateAsync(accountVerifyToken);

                //// oauth account_key update
                //if (registerModel.OAuth != null)
                //{
                //    if ("google".Equals(registerModel.OAuth, StringComparison.OrdinalIgnoreCase))
                //    {
                //        var accountOAuthGoogle = await broker.FindOneAsync<AccountOAuthGoogle>(new SqlCondition<string>("Email", email));
                //        accountOAuthGoogle.Account_Key = accountDomain._key;
                //        await broker.UpdateAsync(accountOAuthGoogle);
                //    }
                //    else if ("naver".Equals(registerModel.OAuth, StringComparison.OrdinalIgnoreCase))
                //    {
                //        var accountOAuthNaver = await broker.FindOneAsync<AccountOAuthNaver>(new SqlCondition<string>("UserID", registerModel.OAuthKey));
                //        accountOAuthNaver.Email = email;
                //        accountOAuthNaver.Account_Key = accountDomain._key;
                //        await broker.UpdateAsync(accountOAuthNaver);
                //    }
                //    else
                //        throw new NotImplementedException($"OAuth - {registerModel.OAuth}");
                //}


                broker.TransactionCommit();
                return accountDomain;
            }
        }

        public async Task<AccountView> FindAccountViewByEmailAsync(string email)
        {
            using (var broker = SqlBroker.CreateNew(ConnectionString))
            {
                return await broker.FindOneAsync<AccountView>(new SqlCondition<string>("Email", email));
            }
        }

        public ApplicationUser GetUser(AccountView accountView)
        {
            var user = new ApplicationUser()
            {
                UserId = accountView.Email,
                UserName = accountView.Username,
                _key = accountView._key
            };
            return user;
        }

        #endregion
    }
}