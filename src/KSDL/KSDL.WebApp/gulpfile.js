'use strict';

var gulp = require('gulp');
var debug = require('gulp-debug');

var paths = {
    src: './wwwsrc/',
    root: './wwwroot/',
    copy: './wwwsrc/copy/',
    lib: './wwwsrc/lib/'
};
paths.exceptCopy = "!" + paths.copy + "**/";

var jade = require('gulp-jade');
paths.jadeSrc = paths.src + '**/*.jade';
gulp.task('jade', function () {
    return gulp.src([paths.exceptCopy, paths.jadeSrc])
        .pipe(debug())
        .pipe(jade({
            doctype: "html",
            pretty: true // 개행됩니다.
        }))
        .pipe(gulp.dest(paths.root))
});

var stylus = require('gulp-stylus');
paths.stylusSrc = paths.src + '**/*.styl';
gulp.task('stylus', function () {
    return gulp.src([paths.exceptCopy, paths.stylusSrc])
        .pipe(debug())
        .pipe(stylus({
            include: ['node_modules'],
            // 'include css': true
            //use:[ jeet(), rupture() ]
            //compress: true,
            //linenos: true,
            //'include css': true,
        }))
        .pipe(gulp.dest(paths.root))
});

var ts = require('gulp-typescript');
var tsProject = ts.createProject('tsconfig.json');
paths.tsSrc = paths.src + '**/*.ts';
gulp.task('ts', function () {
    return gulp.src([
            paths.exceptCopy,
            '!**/**.src.ts', 
            './typings/index.d.ts',
            './typings/_reference.d.ts',           
            paths.tsSrc
        ])
        .pipe(debug())
        .pipe(tsProject())
        .pipe(gulp.dest(paths.root));
});


var modifyFile = require('gulp-modify-file');
var fs = require('fs');
var rename = require("gulp-rename");
paths.mainSrc = [ paths.exceptCopy, paths.src + '**/script.src.ts' ];
gulp.task('vue', function () {
    
    return gulp.src(paths.mainSrc)
        .pipe(modifyFile((content, path, file) => {
            var template = '';
            var script = `<script lang='ts'>${content}</script>`
            var style = '';

            var directory = path.substring(0, path.lastIndexOf("\\") + 1);

            var templatePath = directory + 'template.jade';            
            if (fs.existsSync(templatePath)) {
                var data = fs.readFileSync(templatePath, 'utf-8');
                template = `<template lang='jade'>${data}</template>`
            }

            var stylePath = directory + 'style.styl';
            if (fs.existsSync(stylePath)) {
                var data = fs.readFileSync(stylePath, 'utf-8');
                style = `<style scoped lang='stylus'>${data}</style>`
            }

            var vue = template + script + style;
            return vue;
        }))
        .pipe(rename({ basename: "component", extname: ".vue" }))
        .pipe(gulp.dest(paths.src))
});

var globby = require('globby');
var path = require('path');
var wp = require('webpack');
var webpack = require('webpack-stream');
// paths.packSrc = paths.root + '**/*.src.js';
paths.packSrc = paths.src + '**/main.src.ts';
gulp.task('wp', function () {
    globby.sync(paths.packSrc).forEach(function(filePath) {

        var dir = path.dirname(filePath);        
        dir = dir.startsWith('./wwwsrc') ? dir.substr('./wwwsrc'.length) : dir
        var name = path.basename(filePath);
        if (name.endsWith('.src.ts')) {
            name = name.substr(0, name.indexOf('.src.ts'));
        } else if (name.endsWith('.src.js')) {
            name = name.substr(0, name.indexOf('.src.js'));
        } else {
            return;
        }
        var filename = path.join(dir, name + '.js');
        
        console.log(`Webpack - filePath: ${filePath}(out:${filename})`);

        var result = gulp.src(filePath)
            .pipe(webpack(require('./webpack.config.js')(filename)))
            .pipe(gulp.dest(paths.root));
    });
});

// // vue-copy
// paths.vueCopySrc = paths.src + '**/**/*.vue';
// gulp.task('vue-copy', function () {
//     return gulp.src(paths.vueCopySrc)
//         .pipe(gulp.dest(paths.root))
// });

// copy 폴더를 wwwroot로 복제합니다.
paths.copySrc = paths.copy + '**/*.*';
gulp.task('copy', function () {
    return gulp.src(paths.copySrc)
        .pipe(gulp.dest(paths.root))
});

var runSequence = require('run-sequence');
gulp.task('dz', [], function(cb) {    
    runSequence(['jade', 'stylus', 'ts'],
        ['vue'],
        ['wp']
        );
});

gulp.task('w', function () {
    gulp.watch([
        paths.exceptCopy, 
        paths.tsSrc,
        paths.stylusSrc,
        paths.jadeSrc
    ], ['dz']);
});

// wwwroot 를 모두 삭제합니다.
var rimraf = require('gulp-rimraf');
gulp.task('clean', function (callback) {
    return gulp.src([paths.root + '*', "!" + paths.root + '/lib', "!" + paths.root + '/files'])
        .pipe(rimraf());
});