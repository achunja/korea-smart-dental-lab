﻿using Iyu.Core.Asp.ModernApp.Models;
using Iyu.Data;
using KSDL.Models;
using KSDL.WebApp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSDL.WebApp.Controllers
{
    [Authorize]
    [Route("/api")]
    public class ApiController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly AppBizService bizService;
        private readonly IFileProvider fileProvider;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public ApiController(
            UserManager<ApplicationUser> userManager,
            IBizService bizService,
            IFileProvider fileProvider,
            SignInManager<ApplicationUser> signInManager
            )
        {
            this.userManager = userManager;
            this.bizService = bizService as AppBizService;
            this.fileProvider = fileProvider;
            this._signInManager = signInManager;
        }

        [HttpGet("account-info/{account_key:guid}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAccountInfoAsync(Guid account_key)
        {
            var accountInfo = await bizService.FindOneAsync<AccountInfo>(SqlCondition.Create("Account_key", account_key));
            return Ok(accountInfo);
        }

        [HttpPost("account-info")]
        [AllowAnonymous]
        public async Task<IActionResult> SaveAccountInfoAsync([FromBody] AccountInfo accountInfo)
        {
            if (accountInfo.Account_key == default(Guid)) return BadRequest("required Acount_key");

            var result = await bizService.InsertOrUpdateAsync(accountInfo);
            return Ok();
        }

        [HttpGet("doc-dent-requests/{account_key:guid}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetDocDentRequestsAsync(Guid account_key)
        {
            var items = await bizService.FindAsync<DocDentRequest>(SqlCondition.Create("Account_key", account_key));
            return Ok(items);
        }

        [HttpPost("doc-dent-request")]
        [AllowAnonymous]
        public async Task<IActionResult> SaveDocDentRequestAsync([FromBody] DocDentRequest docDentRequest)
        {
            if (docDentRequest.Account_key == default(Guid)) return BadRequest("required Acount_key");

            var result = await bizService.InsertOrUpdateAsync(docDentRequest);
            return Ok();
        }

        [HttpGet("MasterDentalListItems")]
        [AllowAnonymous]
        public async Task<IActionResult> GetMasterDentalListItemsAsync()
        {
            var items = await bizService.FindAsync<MasterDentalListItem>();
            return Ok(items);
        }
    }
}