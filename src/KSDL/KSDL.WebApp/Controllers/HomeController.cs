﻿using Iyu.Core.Asp.ModernApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSDL.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> userManager;

        public HomeController(IHostingEnvironment env, UserManager<ApplicationUser> userManager)
        {
            _env = env;
            this.userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            return await this.StaticViewAsync();
        }

        [Authorize]
        [Route("/app")]
        public async Task<IActionResult> App()
        {
            if (this.UrlReform(out var url)) return this.RedirectToLocal(url);

            var applicationUser = await userManager.GetUserAsync(User);
            if (applicationUser == null) return Unauthorized();

            var data = new
            {
                account_key = applicationUser._key
            };
            ViewData["Title"] = "App";
            ViewData["Data"] = JsonConvert.SerializeObject(data);

            return View("~/Views/VueApp.cshtml");
        }
    }
}
