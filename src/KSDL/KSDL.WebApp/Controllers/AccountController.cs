﻿using Iyu;
using Iyu.Core.Asp.ModernApp;
using Iyu.Core.Asp.ModernApp.Models;
using Iyu.Core.Asp.ModernApp.Models.AccountViewModels;
using Iyu.Core.Asp.ModernApp.Services;
using Iyu.Data;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSDL.WebApp.Controllers
{
    public enum ErrorCodes
    {
        DuplicateAccount = 30010,
        NotFoundToken = 30020,
        NotMatchedToken = 30030,
        NotFoundUser = 30040
    }

    [Authorize]
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly IIdentityBizService bizService;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
            IIdentityBizService bizService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;

            this.bizService = bizService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            if (this.UrlReform(out var url)) return this.RedirectToLocal(url);

            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ViewData["Title"] = "Login";
            ViewData["ReturnUrl"] = returnUrl;
            return View("~/Views/VueApp.cshtml");
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                _logger.LogInformation("User logged in.");
                return Ok(returnUrl);
            }
            else
            {
                throw new Exception("Not found account");
            }
        }

        [HttpGet("/account/logout")]
        public async Task<IActionResult> LogoutGet()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return RedirectToAction("Index", "Home");
        }

        [HttpPost("/account/logout")]
        public async Task<IActionResult> LogoutPost()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return RedirectToAction("Index", "Home");
        }

        [HttpGet("/account/register-demand")]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterDemandAsync(string email)
        {
            if (string.IsNullOrWhiteSpace(email)) return BadRequest("Required - email");

            var token = await bizService.GetAccountRegisterTokenAsync(email);

            var result = await Task.Run(async () =>
            {
                var title = "Pieces Dental Lab 이메일 확인";
                var subject = $@"
안녕하세요.

Pieces Dental Lab 에 오신 것을 환영합니다.

아래의 링크를 클릭하여 이메일 주소를 확인하고 가입 절차를 완료해 주세요.
http://ksdl.azurewebsites.net/account/register-verification?token={token}

위의 링크를 클릭해도 연결되지 않으면 새 브라우저 창에 URL을 복사하여 붙여넣으세요.

계정에 대해 궁금한 점이 있으면 언제든지
Pieces Dental Lab 계정 고객센터(http://ksdl.azurewebsites.net/support/accounts/)를 방문하세요.

이 메시지는 발신 전용이므로 본 메일에 답장해도
답변을 받을 수 없습니다.
(문의사항은 help@iyulab.com 에 메일을 보내 실 수 있습니다.)
";

                return await _emailSender.SendEmailAsync("KSDL", email, title, subject);
            });

            if (result)
                return Ok($"Send Register Token - {email}");
            else
                throw new Exception("Failed send-email");
        }

        [HttpGet("/account/register")]
        [AllowAnonymous]
        public IActionResult Register()
        {
            if (this.UrlReform(out var url)) return this.RedirectToLocal(url);

            ViewData["Title"] = "Register";
            return View("~/Views/VueApp.cshtml");
        }

        [HttpPost("/account/register")]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterAsync([FromBody]AccountRegisterModel model, string redirectUrl = "/app")
        {
            var account = await bizService.GetAccountByUsernameAsync(model.Username);
            if (account != null)
                throw new CodeException((int)ErrorCodes.DuplicateAccount, "Duplicate Account");

            var accountVerifyToken = await bizService.FindAccountVerifyTokenByTokenAsync(model.Token);
            if (accountVerifyToken == null)
                throw new CodeException((int)ErrorCodes.NotFoundToken, "Not found token");

            if (model.Email.Equals(accountVerifyToken.Email)
                || string.IsNullOrEmpty(model.OAuthKey) != true)
            {
                var pw = CryptoHelper.Encrypt(model.Password, "AccountRegister");
                var accountDomain = await bizService.InsertAccountDomainAsync(model);

                var accountView = await bizService.FindAccountViewByEmailAsync(model.Email);
                if (accountView == null)
                    return NotFound("account");

                var user = bizService.GetUser(accountView);
                await _signInManager.SignInAsync(user, isPersistent: false);
                return this.RedirectToLocal(redirectUrl);
            }
            else
                throw new CodeException((int)ErrorCodes.NotMatchedToken, "Not Matched Token");
        }

        [HttpGet("/account/register-verification")]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterVerificationAsync([FromQuery] Guid token)
        {
            var accountVerifyToken = await bizService.FindAccountVerifyTokenByTokenAsync(token);
            if (accountVerifyToken == null)
                //return BadRequest("Not Found Token");
                throw new CodeException((int)ErrorCodes.NotFoundToken, "Not found token");

            return Redirect($"/account/register?token={token}&email={accountVerifyToken.Email}");
        }
    }
}