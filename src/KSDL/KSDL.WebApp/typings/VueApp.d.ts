interface IAppViewModel {
    isBusy: boolean;
}

interface IAppVue {
    App: IAppViewModel;
}

declare var $VM: IAppVue;