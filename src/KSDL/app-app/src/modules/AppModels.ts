import * as moment from 'moment'

namespace Models {
    abstract class DbModelBase {
        _id: number = 0;
    }
    
    abstract class DbKeyModelBase {
        _key: string = '';
    }

    abstract class DbTsKeyModelBase extends DbKeyModelBase {
        createdDateTime!: string;
        UpdatedDateTime!: string;
    }

    export class AccountInfo extends DbModelBase {
        hospitalName: string = "";
        hospitalPhoneNumber: string = "";
        account_key: string = "";

        constructor(args?: any) {
            super();
            if (args != null) Object.assign(this, args);
        }
    }

    export class DocDentRequest {
    
        hospitalName: string = '';
        orderDate: string;
        requestDate: string;
    
        patientSex: string = '남';
        patientAge: number = 0;
        patientBirthDate: string;
        patientName: string = '';
    
        toothItems: Array<any>;

        account_key: string = '';
    
        constructor() {
            
            this.orderDate = moment().format("YYYY-MM-DD");
            this.requestDate = moment().format("YYYY-MM-DD");
    
            this.patientBirthDate = moment("1990-01-01").format("YYYY-MM-DD");

            this.toothItems = [];
        }
    }

    export class MasterDentalListItem extends DbTsKeyModelBase {
        level!: number;
        parent_key!: string;
        text!: string;
        value!: string;
        isUserValue!: boolean;
    }
}

export default Models;