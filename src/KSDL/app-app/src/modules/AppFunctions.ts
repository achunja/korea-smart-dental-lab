import Models from "./AppModels";
import AppBizService from "./AppBizService";
import VueEngine from "@iyu/VueEngine";

class AppFunctions {

    async getAccountInfoAsync(): Promise<Models.AccountInfo> { 
        var account_key = VueEngine.UI.getAppData("account_key");
        var r = await this.loadAccountInfoAsync(account_key);
        var info =  new Models.AccountInfo(r.data);
        return info;
    } 

    async loadAccountInfoAsync(account_key: string) {
        return await AppBizService.getAccountInfoAsync(account_key);
    }
}

export default new AppFunctions();