import Vue from "vue";
import VueRouter from 'vue-router';

// import Home from "./home/component.vue";
import Dashboard from "./dashboard/component.vue";
import DentRequest from "./dent-request/component.vue";
import MySettings from "./my-settings/component.vue";
// import ProjectPage from "./project-page/component.vue";
// import AgentPage from "./agent-page/component.vue";
// import DocumentPage from "./document-page/component.vue";

Vue.use(VueRouter);
var router = new VueRouter({
	routes: [
		// {
		// 	name: 'Home',
		// 	path: '/',
		// 	component: Home
		// },
		{
			name: 'Dashboard',
			path: '/',
			component: Dashboard
		},		
		{
			name: 'DentRequest',
			path: '/dent-request',
			component: DentRequest
		},
		{
			name: 'MySettings',
			path: '/my-settings',
			component: MySettings
		},		
		// {
		// 	name: 'Agent',
		// 	path: '/:project_key/agent/:agent_key',
		// 	component: AgentPage
		// },
		// {
		// 	name: 'Document',
		// 	path: '/:project_key/views/:view_key',
		// 	component: DocumentPage
		// }
	]
});

export default router;