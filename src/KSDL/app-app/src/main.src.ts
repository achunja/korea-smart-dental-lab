import Vue from 'vue';
import App from './components/App.vue';

import Vuetify from 'vuetify';
Vue.use(Vuetify);
// Vue.config.debug = false;
Vue.config.silent = true;

import * as Html from "@iyu/Html";
import VueEngine from "@iyu/VueEngine";

require('./style.styl');

var run = () => {
    VueEngine.UI.ready();
    
    var vue = new Vue({
        render: h => h(App)
    }).$mount('#app')  
    
    VueEngine.UI.init(vue);
};

Html.importCss("//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic");
Html.importCss("//fonts.googleapis.com/icon?family=Material+Icons");

Html.importJs('//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.8.3.js', () => {
    Html.importJs('//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js', () => {
        Html.importJs('//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js', () => {
            run();            
        });
    });
});