var path = require('path')
var webpack = require('webpack')
var ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

module.exports = {
    context: path.resolve(__dirname),
    entry: './src/main.src.ts',
    resolve: {
        extensions: ['.js', '.json', '.ts', '.vue', '.styl', 'jade'],
        alias: {
            vue$: 'vue/dist/vue.esm.js'
        }
    },
    output: {
        path: path.resolve(__dirname, './dist'),
        publicPath: '/dist/',
        filename: 'build.js'
    },
    module: {
        rules: [
            { test: /\.tsx?$/, loader: 'ts-loader', options: { appendTsSuffixTo: [/\.vue$/], transpileOnly: true } },
            { test: /\.vue$/, loader: 'vue-loader', options: { esModule: true } },
            { test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader?paths=node_modules' },
            { test: /\.(png|jpg|gif|svg)$/, loader: 'file-loader', options: { name: '[name].[ext]?[hash]' } }
        ]
    },
    resolve: {
        extensions: ['.js', '.json', '.ts', '.vue'],
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
            '@src': path.join(__dirname, 'src'),
            '@lib': path.join(__dirname, 'bower_components'),
            '@common': path.join(__dirname, 'common'),
            '@iyu': '@common/Iyu',
            '@modules': '@src/modules'
        },
        symlinks: false
    },
    plugins: [
        new ForkTsCheckerWebpackPlugin({
            // tslint: true,
            // vue: true
        })
    ],
    devServer: {
        historyApiFallback: true,
        noInfo: true
    },
    performance: {
        hints: false
    },
    devtool: '#eval-source-map'
}

if (process.env.NODE_ENV === 'production') {
    module.exports.devtool = '#source-map'
    module.exports.plugins = (module.exports.plugins || []).concat([
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        })
    ])
}