namespace Iyu {

    export function IsNullOrEmpty(value: string)
    {
        return (value == null || value === "");
    }

    export function IsNullOrWhiteSpace(value: string)
    {
        return (value == null || !/\S/.test(value));
    }
}

export default Iyu;