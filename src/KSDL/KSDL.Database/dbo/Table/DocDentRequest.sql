﻿CREATE TABLE [dbo].[DocDentRequest]
(
	[HospitalName] NVARCHAR(100),
	[OrderDate] DATE,
	[RequestDate] DATE,
	[PatientSex] NVARCHAR(2),
	[PatientAge] INT,
	[PatientName] NVARCHAR(20),
	[_id] INT IDENTITY(1,1) PRIMARY KEY,
	[Account_key] UNIQUEIDENTIFIER NOT NULL,
	[CreatedDateTime] DATETIME DEFAULT GETDATE(),
	[UpdatedDateTime] DATETIME 
)