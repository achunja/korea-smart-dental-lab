﻿CREATE TABLE [dbo].[MasterDentalListItem]
(
	[Level] INT NOT NULL,
	[Parent_key] UNIQUEIDENTIFIER, 
    [Text] NVARCHAR(50) NULL, 
    [Value] NVARCHAR(50) NULL, 
    [IsUserValue] BIT NULL DEFAULT 0,
	[_key] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY DEFAULT NEWID(), 
	[CreatedDateTime] DATETIME DEFAULT GETDATE(),
	[UpdatedDateTime] DATETIME 

)
