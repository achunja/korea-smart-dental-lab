﻿CREATE TABLE [dbo].[AccountInfo]
(
	[HospitalName] NVARCHAR(100),
	[HospitalPhoneNumber] NVARCHAR(100),
	[Account_key] UNIQUEIDENTIFIER NOT NULL,
	[_id] INT IDENTITY(1,1) PRIMARY KEY
)
