﻿CREATE VIEW [dbo].[ApplicationUserView]
	AS 
	
	SELECT	_key,
			Email AS UserId,
			Email as UserName,
			[Password] as PasswordHash
	FROM	AccountDomain	