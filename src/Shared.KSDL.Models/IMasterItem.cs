﻿using Iyu.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace KSDL.Models
{
    public interface IMasterItem : IDbModel
    {
    }
    
    //public class MasterDentalFormula : DbTsKeyModelBase, IMasterItem
    //{
    //    public const string _DISPLAY = "치식";
    //    public string Name { get; set; }
    //}

    //public class MasterProstheticsType : DbTsKeyModelBase, IMasterItem
    //{
    //    public const string _DISPLAY = "보철 종류";
    //    public string Name { get; set; }
    //}
    
    //public class MasterProstheticsStructure : DbTsKeyModelBase, IMasterItem
    //{
    //    public const string _DISPLAY = "보철 구조";
    //    public string Name { get; set; }
    //}

    //public class MasterProstheticsShape : DbTsKeyModelBase, IMasterItem
    //{
    //    public const string _DISPLAY = "보철 형태";
    //    public string Name { get; set; }
    //}
}
