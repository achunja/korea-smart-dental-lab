﻿using Iyu.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace KSDL.Models
{
    public class AccountInfo : DbModelBase
    {
        [Display(Name = "병원이름")]
        public string HospitalName { get; set; }
        [Display(Name = "전화번호")]
        public string HospitalPhoneNumber { get; set; }
        [Display(AutoGenerateField = false)]
        public Guid Account_key { get; set; }
    }
}
