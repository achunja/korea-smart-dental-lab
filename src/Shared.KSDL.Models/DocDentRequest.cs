﻿using Iyu;
using Iyu.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace KSDL.Models
{
    public class DocDentRequest : DbTsModelBase
    {
        [Display(Name = "치과명")]
        public string HospitalName { get; set; }
        [Display(Name = "의뢰일자")]
        [Field(FormatString = "yy-MM-dd")]
        public DateTime OrderDate { get; set; }
        [Display(Name = "완성일자")]
        [Field(FormatString = "yy-MM-dd")]
        public DateTime RequestDate { get; set; }

        [Display(Name = "성별")]
        public string PatientSex { get; set; }
        [Display(Name = "연령")]
        public int PatientAge { get; set; }
        [Display(Name = "성명")]
        public string PatientName { get; set; }

        //toothItems: Array<any>;

        [Display(AutoGenerateField = false)]
        public Guid Account_key { get; set; }

        [VirtualField]
        [Display(AutoGenerateField = false)]
        public string Display
        {
            get => $"{this.HospitalName}({this.OrderDate.ToString("yy/MM/dd")}/{this.RequestDate.ToString("yy/MM/dd")}:{PatientName}({PatientAge}))";
        }
    }
}
