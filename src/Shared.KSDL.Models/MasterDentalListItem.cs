﻿using Iyu.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace KSDL.Models
{
    public class MasterDentalListItem : DbTsKeyModelBase, IMasterItem
    {
        public int Level { get; set; }
        public Guid Parent_key { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public bool IsUserValue { get; set; }
    }
}
