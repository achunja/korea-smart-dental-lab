﻿using Iyu.Windows.App;
using Iyu.Windows.App.Enterprise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSDL.App.Workpages
{
    public class DentRequestViewer : WorkpageBase
    {
        public override ILayoutModel CreateLayoutModel()
        {
            return new LayoutModel("기공의뢰서 뷰어", this);
        }
    }
}
