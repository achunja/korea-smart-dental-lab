﻿using Iyu;
using Iyu.Windows.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSDL.App
{
    public static class AppResolver
    {
        public static AppCommands Commands { get => IoC.Container.Resolve<IAppCommands>() as AppCommands; }

        private static AppBizService _Biz;
        public static AppBizService Biz { get => _Biz ?? (_Biz = IoC.Container.Resolve<AppBizService>()); }
    }
}
