﻿using Iyu.Windows.App;
using Iyu.Windows.App.Enterprise;
using KSDL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSDL.App.Panes
{
    internal class DentalHospitalListPane : PaneBase
    {
        public IEnumerable<AccountInfo> ItemsSource { get => GetProperty<IEnumerable<AccountInfo>>(); private set => SetProperty(value); }

        public override ILayoutModel CreateLayoutModel()
        {
            return new LayoutModel("치과 목록", this);
        }

        public override async Task<bool> InitializeAsync()
        {
            await RefreshAsync();

            return await base.InitializeAsync();
        }

        private async Task RefreshAsync()
        {
            this.ItemsSource = await AppResolver.Biz.FindAsync<AccountInfo>();
        }
    }
}
