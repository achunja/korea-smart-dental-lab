﻿using Iyu.Windows;
using Iyu.Windows.App;
using Iyu.Windows.App.Enterprise;
using KSDL.Models;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace KSDL.App.Panes
{
    public class DentalLabRequestListPane : PaneBase
    {
        public override ILayoutModel CreateLayoutModel()
        {
            return new LayoutModel("기공의뢰서 목록", this);
        }

        public ICommand RefreshCommand { get => GetPropertyTry() ?? SetProperty(
            new DelegateCommand(async () =>
            {
                await FillListAsync();
            }));
        }

        public IEnumerable<DocDentRequest> Documents { get => GetProperty<IEnumerable<DocDentRequest>>(); private set => SetProperty(value); }
        public DocDentRequest SelectedDocument
        {
            get => GetProperty<DocDentRequest>();
            set
            {
                SetProperty(value);
                AppManager.Current.SelectedDocDentRequest = value;
            }
        }
        
        private async Task FillListAsync()
        {
            await this.InvokeInBusyAsync(async () =>
            {
                var items = await AppResolver.Biz.FindAsync<DocDentRequest>();
                this.Documents = items;
            });
        }

        public override async Task<bool> InitializeAsync()
        {
            await FillListAsync();
            return await base.InitializeAsync();
        }
    }
}
