﻿using Iyu;
using Iyu.Windows.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSDL.App
{
    public abstract class AppBootstrapperBase : BootstrapperBase
    {
        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();

            IoC.Container.Register<IAppCommands>(IoC.Container.Resolve<AppCommands>());
            IoC.Container.Register<AppBizService>(IoC.Container.Resolve<AppBizService>());
        }

        public override IAppManager CreateAppManager()
        {
            return new AppManager();
        }

        public override Type RegisterAppShell()
        {
            return typeof(AppShell);
        }
    }
}
