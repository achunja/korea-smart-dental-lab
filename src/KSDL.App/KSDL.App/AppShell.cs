﻿using Iyu.Windows;
using Iyu.Windows.App;
using Iyu.Windows.App.Enterprise;
using KSDL.App.UI;
using Prism.Regions;
using System.Windows.Controls;

namespace KSDL.App
{
    public class AppShell : EnterpriseAppShell
    {
        public AppShell(RegionManager regionManager) : base(regionManager)
        {
            this.LoginVM = new LoginViewModel();
            this.LoginVM.LoginPassed += (s, e) =>
            {
                MaterialDesignThemes.Wpf.Transitions.Transitioner.MoveNextCommand.Execute(null, null);
            };
        }

        public override RegionTypes CreateRegionTypes()
        {
            return new RegionTypes()
            {
                MainMenuViewType = typeof(KSDL.App.Regions.Shell.MainMenuRegion),
                ToolBarViewType = typeof(KSDL.App.Regions.Shell.ToolBarRegion),
            };
        }
    }
}
