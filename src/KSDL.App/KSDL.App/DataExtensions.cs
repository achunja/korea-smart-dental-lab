﻿using Iyu.Data;
using KSDL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSDL.App
{
    public static class DataExtensions
    {
        public static AppBizService Biz => AppResolver.Biz;

        public static async Task<int> UpdateAsync<T>(this ObjectCollectionView<T> coll)
            where T: class, IDbModel, new()
        {
            var items = coll.GetChanges();
            return await Biz.UpdatesAsync(items);
        }
    }
}
