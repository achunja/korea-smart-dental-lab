﻿using Iyu.Windows;
using Iyu.Windows.App.Enterprise.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSDL.App.UI
{
    public class LoginViewModel : LoginViewModelBase
    {
        public override void InitTitle(ref string title, ref string subtitle)
        {   
            title = "Pieces Dental Lab";
            subtitle = "Welcome Korea Smart Dental Lab";
        }

        public override Task<bool> LoginTryAsync(string id, string password)
        {
            return Task.FromResult(true);
        }
    }
}
