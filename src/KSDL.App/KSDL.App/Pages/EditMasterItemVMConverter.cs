﻿using KSDL.App.ViewModels;
using KSDL.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace KSDL.App.Pages
{
    public class EditMasterItemVMConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string s)
                return ConvertToVM(s);
            else
                return Binding.DoNothing;

        }

        internal static IEditViewModel ConvertToVM(string name)
        {
            //if (name.Equals(MasterDentalFormula._DISPLAY))
            //    return new DentalFormulaEditViewModel();

            //else if (name.Equals(MasterProstheticsType._DISPLAY))
            //    return new ProstheticsTypeEditViewModel();

            //else if (name.Equals(MasterProstheticsStructure._DISPLAY))
            //    return new ProstheticsStructureEditViewModel();

            //else if (name.Equals(MasterProstheticsShape._DISPLAY))
            //    return new ProstheticsShapeEditViewModel();

            //else
            //    throw new NotImplementedException();
            throw new NotImplementedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}