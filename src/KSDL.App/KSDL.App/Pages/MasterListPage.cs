﻿using Iyu.Windows;
using Iyu.Windows.App;
using KSDL.App.ViewModels;
using KSDL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSDL.App.Pages
{
    public class MasterListPage : DialogBase
    {
        public override IWindowSettings CreateWindowSettings()
        {
            return new WindowSettings(this, "기준자료 목록");
        }

        public IEnumerable<string> MasterItems { get => GetProperty<IEnumerable<string>>(); set => SetProperty(value); }
        public string SelectedMasterItem
        {
            get => GetProperty<string>();
            set
            {
                SetProperty(value);
                this.EditMasterItemVM = EditMasterItemVMConverter.ConvertToVM(value);
            }
        }

        public IEditViewModel EditMasterItemVM { get => GetProperty<IEditViewModel>(); private set => SetProperty(value); }

        public override async Task<bool> InitializeAsync()
        {
            await RefreshMasterItemsAsync();

            return await base.InitializeAsync();
        }

        private Task RefreshMasterItemsAsync()
        {
            this.MasterItems = new string[]
            {
                //MasterDentalFormula._DISPLAY,
                //MasterProstheticsType._DISPLAY,
                //MasterProstheticsStructure._DISPLAY,
                //MasterProstheticsShape._DISPLAY
            };

            return Task.FromResult(true);
        }

        public override void OnExecuteOkCommand(object p)
        {
            if (this.EditMasterItemVM != null)
            {
                this.EditMasterItemVM.OnExecuteOkAsync();
            }
            //base.OnExecuteOkCommand(p);
        }
    }
}
