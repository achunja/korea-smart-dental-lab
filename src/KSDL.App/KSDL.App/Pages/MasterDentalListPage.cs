﻿using Iyu.Windows;
using Iyu.Windows.App;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KSDL.Models;
using System.Windows.Data;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Prism.Commands;

namespace KSDL.App.Pages
{
    public class MasterDentalListPage : DialogBase
    {
        public override IWindowSettings CreateWindowSettings()
        {
            return new WindowSettings(this, "[기준자료 편집] 기공종목 및 재료");
        }

        public ObservableCollection<MasterDentalListItem> ListSource { get => GetProperty<ObservableCollection<MasterDentalListItem>>(); set => SetProperty(value); }
        public ICollectionView ItemsLv1View { get => GetProperty<ICollectionView>(); set => SetProperty(value); }
        public ICollectionView ItemsLv2View { get => GetProperty<ICollectionView>(); set => SetProperty(value); }
        public ICollectionView ItemsLv3View { get => GetProperty<ICollectionView>(); set => SetProperty(value); }
        public ICollectionView ItemsLv4View { get => GetProperty<ICollectionView>(); set => SetProperty(value); }
        public ICollectionView ItemsLv5View { get => GetProperty<ICollectionView>(); set => SetProperty(value); }
        public ICollectionView ItemsLv6View { get => GetProperty<ICollectionView>(); set => SetProperty(value); }
        public ICollectionView ItemsLv7View { get => GetProperty<ICollectionView>(); set => SetProperty(value); }

        public MasterDentalListItem SelectedItemLv1 { get => GetProperty<MasterDentalListItem>(); set { SetProperty(value); RefreshList(1); } }
        public MasterDentalListItem SelectedItemLv2 { get => GetProperty<MasterDentalListItem>(); set { SetProperty(value); RefreshList(2); } }
        public MasterDentalListItem SelectedItemLv3 { get => GetProperty<MasterDentalListItem>(); set { SetProperty(value); RefreshList(3); } }
        public MasterDentalListItem SelectedItemLv4 { get => GetProperty<MasterDentalListItem>(); set { SetProperty(value); RefreshList(4); } }
        public MasterDentalListItem SelectedItemLv5 { get => GetProperty<MasterDentalListItem>(); set { SetProperty(value); RefreshList(5); } }
        public MasterDentalListItem SelectedItemLv6 { get => GetProperty<MasterDentalListItem>(); set { SetProperty(value); RefreshList(6); } }
        public MasterDentalListItem SelectedItemLv7 { get => GetProperty<MasterDentalListItem>(); set { SetProperty(value); RefreshList(7); } }

        public ICommand AddCommand
        {
            get => GetPropertyTry() ?? SetProperty(new DelegateCommand<object>((p) =>
            {
                var lv = Convert.ToInt32(p);
                var newItem = new MasterDentalListItem() { Level = lv, _key = Guid.NewGuid() };
                newItem.SetBindingStatus(Iyu.Data.BindingStatus.Created);

                try
                {
                    if (lv == 2)
                        newItem.Parent_key = SelectedItemLv1._key;
                    else if (lv == 3)
                        newItem.Parent_key = SelectedItemLv2._key;
                    else if (lv == 4)
                        newItem.Parent_key = SelectedItemLv3._key;
                    else if (lv == 5)
                        newItem.Parent_key = SelectedItemLv4._key;
                    else if (lv == 6)
                        newItem.Parent_key = SelectedItemLv5._key;
                    else if (lv == 7)
                        newItem.Parent_key = SelectedItemLv6._key;

                }
                catch (Exception)
                {
                    return;
                }

                ListSource.Add(newItem);
            }));
        }

        public override async Task<bool> InitializeAsync()
        {
            await Refresh();
            return await base.InitializeAsync();
        }

        private async Task Refresh()
        {
            var items = await AppResolver.Biz.FindAsync<MasterDentalListItem>();
            this.SetProperty(items.Select(p => p._key).ToArray(), "original-items");

            this.ListSource = new ObservableCollection<MasterDentalListItem>(items);

            ItemsLv1View = new CollectionViewSource() { Source = ListSource }.View;
            ItemsLv1View.SortDescriptions.Add(new SortDescription("Text", ListSortDirection.Ascending));
            ItemsLv1View.Filter = (p) => { return ((MasterDentalListItem)p).Level == 1; };

            ItemsLv2View = new CollectionViewSource() { Source = ListSource }.View;
            ItemsLv2View.SortDescriptions.Add(new SortDescription("Text", ListSortDirection.Ascending));
            ItemsLv2View.Filter = (p) => { return ((MasterDentalListItem)p).Level == 2 && ((MasterDentalListItem)p).Parent_key == SelectedItemLv1?._key; };

            ItemsLv3View = new CollectionViewSource() { Source = ListSource }.View;
            ItemsLv3View.SortDescriptions.Add(new SortDescription("Text", ListSortDirection.Ascending));
            ItemsLv3View.Filter = (p) => { return ((MasterDentalListItem)p).Level == 3 && ((MasterDentalListItem)p).Parent_key == SelectedItemLv2?._key; };

            ItemsLv4View = new CollectionViewSource() { Source = ListSource }.View;
            ItemsLv4View.SortDescriptions.Add(new SortDescription("Text", ListSortDirection.Ascending));
            ItemsLv4View.Filter = (p) => { return ((MasterDentalListItem)p).Level == 4 && ((MasterDentalListItem)p).Parent_key == SelectedItemLv3?._key; };

            ItemsLv5View = new CollectionViewSource() { Source = ListSource }.View;
            ItemsLv5View.SortDescriptions.Add(new SortDescription("Text", ListSortDirection.Ascending));
            ItemsLv5View.Filter = (p) => { return ((MasterDentalListItem)p).Level == 5 && ((MasterDentalListItem)p).Parent_key == SelectedItemLv4?._key; };

            ItemsLv6View = new CollectionViewSource() { Source = ListSource }.View;
            ItemsLv6View.SortDescriptions.Add(new SortDescription("Text", ListSortDirection.Ascending));
            ItemsLv6View.Filter = (p) => { return ((MasterDentalListItem)p).Level == 6 && ((MasterDentalListItem)p).Parent_key == SelectedItemLv5?._key; };

            ItemsLv7View = new CollectionViewSource() { Source = ListSource }.View;
            ItemsLv7View.SortDescriptions.Add(new SortDescription("Text", ListSortDirection.Ascending));
            ItemsLv7View.Filter = (p) => { return ((MasterDentalListItem)p).Level == 7 && ((MasterDentalListItem)p).Parent_key == SelectedItemLv6?._key; };
        }

        private void RefreshList(int lv)
        {   
            if (lv < 7)
            {
                if (lv < 6)
                {
                    if (lv < 5)
                    {
                        if (lv < 4)
                        {
                            if (lv < 3)
                            {
                                if (lv < 2)
                                {
                                    ItemsLv2View?.Refresh();
                                }
                                ItemsLv3View?.Refresh();
                            }
                            ItemsLv4View?.Refresh();
                        }
                        ItemsLv5View?.Refresh();
                    }
                    ItemsLv6View?.Refresh();
                }
                ItemsLv7View?.Refresh();
            }
        }

        public override async void OnExecuteOkCommand(object p)
        {
            await this.InvokeInBusyAsync(async () =>
            {
                var originalKeys = this.GetProperty<IEnumerable<Guid>>("original-items");
                var currentKeys = this.ListSource.Select(item => item._key);
                var removedKeys = originalKeys.Except(currentKeys);
                foreach (var key in removedKeys)
                {
                    await AppResolver.Biz.DeleteByKeysAsync(nameof(MasterDentalListItem), removedKeys);
                }

                var items = this.ListSource.Where((item) => string.IsNullOrWhiteSpace(item.Text) != true && string.IsNullOrWhiteSpace(item.Value) != true);
                await AppResolver.Biz.UpdatesAsync(items);
                await Refresh();
            });
        }
    }
}
