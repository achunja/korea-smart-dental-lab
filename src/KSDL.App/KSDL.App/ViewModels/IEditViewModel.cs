﻿using Iyu.Windows;
using KSDL.Models;
using Prism.Commands;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace KSDL.App.ViewModels
{
    public interface IEditViewModel
    {
        Task<bool> OnExecuteOkAsync();
    }

    public abstract class EditViewModelBase : ViewModelBase, IEditViewModel
    {
        public abstract Task<bool> OnExecuteOkAsync();
    }

    /*
    public class DentalFormulaEditViewModel : EditViewModelBase
    {
        public ObjectCollectionView<MasterDentalFormula> Items { get => GetProperty<ObjectCollectionView<MasterDentalFormula>>(); set => SetProperty(value); }

        public DentalFormulaEditViewModel()
        {
            this.Items = new ObjectCollectionView<MasterDentalFormula>(new List<MasterDentalFormula>());
            RefreshAsync();
        }

        private async void RefreshAsync()
        {
            await this.InvokeInBusyAsync(async () =>
            {
                this.Items.Clear();
                var items = await AppResolver.Biz.FindAsync<MasterDentalFormula>();
                items.ForEach(p => this.Items.AddItem(p));
            });
        }

        public ICommand AddItemCommand => GetPropertyTry() ?? SetProperty(
            new DelegateCommand(() =>
            {
                var item = this.Items.AddNew();
                item.Name = "새 치식";
            }));

        public override async Task<bool> OnExecuteOkAsync()
        {
            await this.Items.UpdateAsync();
            return true;
        }
    }

    public class ProstheticsTypeEditViewModel : EditViewModelBase
    {
        public ObjectCollectionView<MasterProstheticsType> Items { get => GetProperty<ObjectCollectionView<MasterProstheticsType>>(); set => SetProperty(value); }

        public ProstheticsTypeEditViewModel()
        {
            this.Items = new ObjectCollectionView<MasterProstheticsType>(new List<MasterProstheticsType>());
            RefreshAsync();
        }

        private async void RefreshAsync()
        {
            var items = await AppResolver.Biz.FindAsync<MasterProstheticsType>();
            items.ForEach(p => this.Items.AddItem(p));
        }

        public ICommand AddItemCommand => GetPropertyTry() ?? SetProperty(
            new DelegateCommand(() =>
            {
                var item = this.Items.AddNew();
                item.Name = "새 보철종류";
            }));

        public override async Task<bool> OnExecuteOkAsync()
        {
            await this.Items.UpdateAsync();
            return true;
        }
    }

    public class ProstheticsStructureEditViewModel : EditViewModelBase
    {
        public ObjectCollectionView<MasterProstheticsStructure> Items { get => GetProperty<ObjectCollectionView<MasterProstheticsStructure>>(); set => SetProperty(value); }

        public ProstheticsStructureEditViewModel()
        {
            this.Items = new ObjectCollectionView<MasterProstheticsStructure>(new List<MasterProstheticsStructure>());
            RefreshAsync();
        }

        private async void RefreshAsync()
        {
            var items = await AppResolver.Biz.FindAsync<MasterProstheticsStructure>();
            items.ForEach(p => this.Items.AddItem(p));
        }

        public ICommand AddItemCommand => GetPropertyTry() ?? SetProperty(
            new DelegateCommand(() =>
            {
                var item = this.Items.AddNew();
                item.Name = "새 보철구조";
            }));

        public override async Task<bool> OnExecuteOkAsync()
        {
            await this.Items.UpdateAsync();
            return true;
        }
    }

    public class ProstheticsShapeEditViewModel : EditViewModelBase
    {
        public ObjectCollectionView<MasterProstheticsShape> Items { get => GetProperty<ObjectCollectionView<MasterProstheticsShape>>(); set => SetProperty(value); }

        public ProstheticsShapeEditViewModel()
        {
            this.Items = new ObjectCollectionView<MasterProstheticsShape>(new List<MasterProstheticsShape>());
            RefreshAsync();
        }

        private async void RefreshAsync()
        {
            var items = await AppResolver.Biz.FindAsync<MasterProstheticsShape>();
            items.ForEach(p => this.Items.AddItem(p));
        }

        public ICommand AddItemCommand => GetPropertyTry() ?? SetProperty(
            new DelegateCommand(() =>
            {
                var item = this.Items.AddNew();
                item.Name = "새 보철형태";
            }));

        public override async Task<bool> OnExecuteOkAsync()
        {
            await this.Items.UpdateAsync();
            return true;
        }
    }
    */
}
