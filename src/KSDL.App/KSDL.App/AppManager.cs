﻿using Iyu;
using Iyu.Windows.App;
using KSDL.Models;

namespace KSDL.App
{
    public class AppManager : AppManagerBase
    {
        private static AppManager _Current;
        public static AppManager Current => _Current ?? (_Current = IoC.Container.Resolve<IAppManager>() as AppManager);

        public DocDentRequest SelectedDocDentRequest
        {
            get => GetProperty<DocDentRequest>();
            set => SetProperty(value);
        }
    }
}
